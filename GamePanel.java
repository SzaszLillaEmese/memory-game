package masodik_nekifutas;

import java.awt.Color;

import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

// a View
public class GamePanel extends JPanel{
	private Card [][]cards;
	private int n;
	private int level;
	private int imagesSize;
	private BufferedImage []images;
	private BufferedImage back;
	private int panelSize;
	private GameController gameController;
	private JFrame frame;
	
	public GamePanel(GameController gc, JFrame f) {
		frame = f;
		frame.setResizable(false);
		gameController = gc;
		cards = gameController.getCards();
		n = gameController.getCardsSize();
		level = gameController.getLevel();
		imagesSize = gameController.getImagesSize();
		this.setBackground(Color.darkGray);
		panelSize = n * 64 + (n+1) * 10;
		this.setSize(panelSize,panelSize);
		images = new BufferedImage[imagesSize];
		for (int i = 0; i < imagesSize; i++) {
			try {
				Integer in = i;
				images[i] = ImageIO.read(
						new File("src\\masodik_nekifutas\\Images\\" + in.toString() + ".png")
						);
			}         
			catch(IOException e){
				e.printStackTrace();       
			}
		}
		try {
			back = ImageIO.read(
					new File("src\\masodik_nekifutas\\Images\\back.png")
					);
		}         
		catch(IOException e){
			e.printStackTrace();       
		}
		
		this.setVisible(true);
	}
	
	public int getGamePanelSize() {
		return panelSize;
	}
	
	public void FilesMenuListener() {
		
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_S, 0), "save");
		getRootPane().getActionMap().put("save", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameController.saveGame();
            }
        });
		
		getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_L, 0), "load");
		getRootPane().getActionMap().put("load", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                gameController.loadGame();
                repaint();
            }
        });
		
	}
	
	public void GamePanelListener() {
		this.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				gameController.whenMouseClicked(e);
				repaint();
				gameController.changeClickedCards();
				repaint();
				if (gameController.isGameWon()) {
					//System.out.println("Game is won!");
					new WinnerPanel();
					frame.dispose();
				}
				
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}
	
	@Override
	public void paintComponent(Graphics g) {
		int x = 10, y = 10;
		Color myNewColor = new Color (240, 240, 255);
		g.setColor(myNewColor);
		g.fillRect(0, 0, panelSize, panelSize);
		for (int i = 0; i < n; i++) {
			y = i * (64 + 10);
			for (int j = 0; j < n; j++) {
				x =  j * (64 + 10);
				if (cards[i][j].getVisible()) {
					g.drawImage(images[cards[i][j].getImgId()],x,y,null);
				} else {
					g.drawImage(back,x,y,null);
				}
			}
		}
	}
	
}
