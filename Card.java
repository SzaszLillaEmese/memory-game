package masodik_nekifutas;

// a model
public class Card {
	private boolean visible;
	private int imgId;
	
	public Card(boolean v, int id) {
		visible = v;
		imgId = id;
	}
	
	public void setImgId(int id) {
		imgId = id;
	}
	
	public void setVisible(boolean v) {
		visible = v;
	}
	
	public boolean getVisible() {
		return visible;
	}
	
	public int getImgId() {
		return imgId;
	}
	
}
