package masodik_nekifutas;

import java.awt.Color;

import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class MenuPanel extends JFrame{
	private JLabel nameOfGame;
	private JButton play, howTo, exit;
	private JFrame menuFrame;
	private int level = 2;
	JPanel diffPanel = new JPanel();
	String []difficulties = {"2", "3"};
	JComboBox diff = new JComboBox(difficulties);
	
	public MenuPanel(/*JFrame mf*/) {
		//menuFrame = mf;
		this.setTitle("MemoryGame");
		setSize(600,600);
		Font myFont = new Font("Papyrus", Font.BOLD, 50);
		JPanel menuPanel = new JPanel();
		menuPanel.setSize(600, 400);
		menuPanel.setBackground(Color.darkGray);
		menuPanel.setLayout(new GridLayout(5,1));
		nameOfGame = new JLabel("MEMORY GAME",SwingConstants.CENTER);
		nameOfGame.setFont(myFont);
		nameOfGame.setBackground(Color.black);
		nameOfGame.setForeground(Color.white);
		play = new JButton("PLAY");
		play.setFont(myFont);
		play.setForeground(Color.white);
		howTo = new JButton("HOW TO PLAY");
		howTo.setFont(myFont);
		howTo.setForeground(Color.white);
		exit = new JButton("EXIT");
		exit.setFont(myFont);
		exit.setForeground(Color.white);
		play.setBackground(Color.darkGray);
		howTo.setBackground(Color.darkGray);
		exit.setBackground(Color.darkGray);
		JLabel diffLabel = new JLabel("Difficulty:");
		diffLabel.setFont(myFont);
		diffLabel.setBackground(Color.darkGray);
		diffLabel.setForeground(Color.white);
		diffPanel.setBackground(Color.darkGray);
		diffPanel.add(diffLabel); diffPanel.add(diff);
		menuPanel.add(nameOfGame);
		menuPanel.add(play);
		menuPanel.add(diffPanel);
		menuPanel.add(howTo);
		menuPanel.add(exit);
		add(menuPanel);
		setVisible(true);
		
	}
	
	public void menuPanelListener() {
		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
				//System.out.println("ExitButton was clicked!");
			}
			
		});
		
		MenuPanel mp = this;
		howTo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				//System.out.println("HowToButton was clicked!");
				HowToPanel howToPanel = new HowToPanel(menuFrame, mp);
				dispose();
			}
			
		});
		
		diff.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String s = (String) diff.getSelectedItem();
				if (s.equals("2")) level = 2;
				else level = 3;
			}
			
		});
		
		
		play.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				JFrame frame = new JFrame();
				frame.setTitle("Level "+level);
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				int number;
				if (level == 3) {
					number = 9;
				} else number = 6;
				GameController g = new GameController(number,level);
				GamePanel p = new GamePanel(g,frame);

				frame.setSize(p.getGamePanelSize()+5, p.getGamePanelSize()+30);
				frame.add(p);
				p.GamePanelListener();
				p.FilesMenuListener();
				
				
				frame.setVisible(true);
				dispose();
			}
			
		});

		
	}
	
	public JFrame getMenuFrame() {
		return menuFrame;
	}
	
	

}
