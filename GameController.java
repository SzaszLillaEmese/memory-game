package masodik_nekifutas;

import java.applet.Applet;

import java.applet.AudioClip;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.swing.JFileChooser;

public class GameController {
	private Card [][]cards;
	private int n;
	private int level;
	private int imagesSize;
	private ArrayList<Card> clickedCards = new ArrayList<Card>();
	AudioClip wowClip;
	
	public GameController (int nn, int l) {
		n = nn;
		level = l;
		imagesSize = n * n / level;
		cards = new Card[n][n];
		Random rand = new Random();
		int randomImgId;
		int  []times = new int[imagesSize];
		for (int i = 0; i < imagesSize; i++) times[i] = 0;
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				while (true) {
					randomImgId = rand.nextInt(imagesSize);
					if (times[randomImgId]<level) {
						times[randomImgId]++;
						cards[i][j] = new Card(false,randomImgId);
						break;
					}
				}
			}
		}

		File auFile = new File("src\\masodik_nekifutas\\Images\\sound\\wow.au");
		wowClip = new AudioClip() {

			@Override
			public void play() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void loop() {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void stop() {
				// TODO Auto-generated method stub
				
			}
			
		};
		
		try {
			wowClip = Applet.newAudioClip(auFile.toURL());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public void saveGame() {
		JFileChooser fileChooser = new JFileChooser("src\\masodik_nekifutas\\Saves");
		try {
			fileChooser.showSaveDialog(null);
			FileWriter fw = new FileWriter(fileChooser.getSelectedFile()+".txt");
			fw.write(getSaveInformation());
			fw.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public void loadGame() {
		JFileChooser fileChooser = new JFileChooser("src\\masodik_nekifutas\\Saves");
		try {
			fileChooser.showOpenDialog(null);
			Scanner fs = new Scanner(fileChooser.getSelectedFile());
			String line = "";
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					boolean vis;
					line = fs.nextLine();
					int id = Integer.parseInt(line);
					line = fs.nextLine();
					int vis_ = Integer.parseInt(line);
					if (vis_ == 0) vis = false;
					else vis = true;
					cards[i][j].setImgId(id);
					cards[i][j].setVisible(vis);
				}
				
				
			}
			
			fs.close();
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getSaveInformation() {
		String info = "";
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				info = info + cards[i][j].getImgId() + "\n";
				if (cards[i][j].getVisible()) info = info + "1" + "\n";
				else info = info + "0" + "\n";
			}
		}
		return info;
	}
	
	
	public void whenMouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		// int panelsize = e.getComponent().getHeight(); 
		int panelheight = e.getComponent().getHeight(), panelwidth = e.getComponent().getWidth();
		int j = x / (panelheight / n);
		int i = y / (panelwidth / n);
		if (!cards[i][j].getVisible()) {
			cards[i][j].setVisible(true);
			clickedCards.add(cards[i][j]);
		}
		
	}
	
	public void changeClickedCards() {
		if (clickedCards.size() == level) {
			boolean good = true;
			Card previous = clickedCards.get(0);
			Card c = null;
			for (int i = 1; i < clickedCards.size(); i++) {
				c = clickedCards.get(i);
				if (previous.getImgId() != c.getImgId()) good = false;
				previous = c;
			}
			if (good) {
				wowClip.play();
				int size = clickedCards.size();
				for (int i1 = size-1; i1 >=0; i1--) {
					//clickedCards.get(i1).setVisible(false);
					clickedCards.remove(i1);
				}
			}
		}
		else if (clickedCards.size() == level + 1) {
			int size = clickedCards.size();
			for (int i1 = size-1; i1 >=0; i1--) {
				clickedCards.get(i1).setVisible(false);
				clickedCards.remove(i1);
			}
		}
		
		
	}
	
	public boolean isGameWon() {
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				if (!cards[i][j].getVisible()) return false;
			}
		}
		return true;
	}
	
	public Card[][] getCards(){
		return cards;
	}
	
	public int getCardsSize() {
		return n;
	}
	
	public int getLevel() {
		return level;
	}
	
	public int getImagesSize() {
		return imagesSize;
	}

}
