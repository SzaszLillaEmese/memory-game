package masodik_nekifutas;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class HowToPanel extends JFrame{
	private MenuPanel menuPanel;
	private JFrame menuFrame;
	private JLabel howToPlay;
	private JButton back;
	
	public HowToPanel(JFrame mf, MenuPanel mp) {
		menuPanel = mp;
		//menuFrame = mf;
		setTitle("How To Play");
		this.setSize(menuPanel.getWidth(), menuPanel.getHeight());
		
		Font myFont = new Font("Papyrus", Font.BOLD, 30);
		howToPlay = new JLabel("<html>-Choose difficulty level (2 or 3) <br/> -Try to pair up the cards with the same symbol on them <br/> - Press s to and press l to load</html>", SwingConstants.CENTER);
		howToPlay.setBackground(Color.darkGray);
		howToPlay.setForeground(Color.white);
		howToPlay.setFont(myFont);
		back = new JButton("BACK");
		back.setFont(myFont);
		back.setForeground(Color.white);
		back.setBackground(Color.darkGray);
		
		JPanel howToPanel = new JPanel();
		howToPanel.setSize(menuPanel.getWidth(), menuPanel.getHeight());
		howToPanel.setBackground(Color.darkGray);
		
		howToPanel.setLayout(new GridLayout(2,1));
		howToPanel.add(howToPlay);
		howToPanel.add(back);
		
		howToPanel.setVisible(true);
		
		add(howToPanel);
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				MenuPanel mp = new MenuPanel();
				mp.menuPanelListener();
			}
			
		});
		
		howToPanel.setVisible(true);
		
		add(howToPanel);
		setVisible(true);
		
	}

}
