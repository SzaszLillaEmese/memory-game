# Memory Game

Simple Memory Game in Java using Swing and Model-View-Controller structure
Card being the Model
GamePanel being the View
GameController being the Controller

You can choose difficulty by selecting 2 or 3 in the menu:
    - 2 means you have to match 2 cards
    - 3 means you have to match 3 cards in order to make a pair ... trio?
    - you can press s to save current game and can press l to load a game
    - wow sound plays whenever a match is made

