package masodik_nekifutas;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class WinnerPanel extends JFrame{
	private JPanel winnerPanel;
	private JLabel winnerLabel;
	private JButton exit, back;
	
	public WinnerPanel() {
		setTitle("You Won!");
		setSize(600,300);
		winnerPanel = new JPanel();
		winnerPanel.setBackground(Color.darkGray);
		winnerPanel.setLayout(new GridLayout(2,1));
		winnerPanel.setSize(400,200);
		Font myFont = new Font("Papyrus", Font.BOLD, 20);
		Font myFont2 = new Font("Papyrus", Font.BOLD, 30);
		winnerLabel = new JLabel("GAME IS WON! GOOD JOB!",SwingConstants.CENTER);
		winnerLabel.setForeground(Color.white);
		winnerLabel.setFont(myFont2);
		exit = new JButton("EXIT");
		exit.setFont(myFont);
		exit.setBackground(Color.darkGray);
		exit.setForeground(Color.white);
		back = new JButton("BACK TO MENU");
		back.setFont(myFont);
		back.setBackground(Color.darkGray);
		back.setForeground(Color.white);
		
		JPanel choicePanel = new JPanel();
		choicePanel.setBackground(Color.darkGray);
		choicePanel.setLayout(new GridLayout(1,2));
		choicePanel.add(back); choicePanel.add(exit);
		
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dispose();
				MenuPanel mp = new MenuPanel();
				mp.menuPanelListener();
			}
			
		});
		
		exit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.exit(0);
				//System.out.println("ExitButton was clicked!");
			}
			
		});
		
		winnerPanel.add(winnerLabel); winnerPanel.add(choicePanel);
		add(winnerPanel);
		
		setVisible(true);
	}
	

}
